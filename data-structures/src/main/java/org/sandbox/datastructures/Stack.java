package org.sandbox.datastructures;

public interface Stack<T> extends AbstractDataType {

    public abstract void push(T item);

    public abstract T top();

    public abstract T pop();

    public abstract int size();

    public abstract boolean isEmpty();
}