package org.sandbox.datastructures;

import java.util.EmptyStackException;


public class ArrayStack<T> implements Stack<T> {

    private T[] arr;
    private int size;

    @SuppressWarnings("unchecked")
    public ArrayStack(int maxSize) {
        this.arr = (T[]) new Object[maxSize];
    }

    public void push(T item) {
        if (size == this.arr.length) {
            throw new StackOverflowError();
        }

        this.arr[size] = item;
        this.size++;
    }

    public T top() {
        if (size == 0) {
            throw new EmptyStackException();
        }

        return this.arr[size - 1];
    }

    public T pop() {
        if (size == 0) {
            throw new EmptyStackException();
        }

        T item = this.arr[size - 1];
        this.arr[size - 1] = null;
        this.size--;

        return item;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }
}
