This is project represents a coding sandbox in Java.

It serves for:
- Learning Java basics
- Learning and implementing algorithms in Java
- Learning and implementing data structures in Java
- Solving various problems in Java
- Playing with different technologies in Java