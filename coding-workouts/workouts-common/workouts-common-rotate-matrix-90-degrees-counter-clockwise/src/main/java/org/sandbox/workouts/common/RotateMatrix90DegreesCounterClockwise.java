package org.sandbox.workouts.common;

import java.util.Arrays;

public class RotateMatrix90DegreesCounterClockwise {

    public static void main(String... args) {
        int size = 4;
        int[][] mat = generateSquareMatrix(size);
        int[][] rotationMatrix = {
                {0, 0, 0, 1},
                {0, 0, 1, 0},
                {0, 1, 0, 0},
                {1, 0, 0, 0}
        };
        int[][] cp = generateSquareMatrix(size);

        System.out.println("Number of rows in matrix: " + mat.length);
        System.out.println("Number of columns in matrix: " + mat[0].length);
        System.out.println("Size of a matrix: " + mat.length * mat[0].length);
        System.out.println("\nMatrix: " + Arrays.deepToString(mat) + "\n");
        printMat(mat);

        System.out.println("\nRotation Matrix: " + Arrays.deepToString(rotationMatrix) + "\n");
        printMat(rotationMatrix);

        mat = multiplySquareMatrices(mat, rotationMatrix);
        System.out.println("\n\nMatrix after multiplying by linear transformation matrix once: ");
        printMat(mat);

        mat = multiplySquareMatrices(mat, rotationMatrix);
        System.out.println("\n\nMatrix after multiplying by linear transformation matrix twice: ");
        printMat(mat);

        System.out.println("Matrix after transpose: ");
        transposeMat(cp);
        printMat(cp);

        // Pivot the matrix around its center row
        int tmp;
        for (int i = 0; i < cp.length / 2; i++) {
            for (int j = 0; j < cp.length; j++) {
                tmp = cp[i][j];
                cp[i][j] = cp[cp.length - i - 1][j];
                cp[cp.length - i - 1][j] = tmp;
            }
        }
        System.out.println("\n\nMatrix after 90 degrees rotation counter-clockwise: ");
        printMat(cp);



        System.out.println("\nMatrix after rotation: " + Arrays.deepToString(mat) + "\n");
        printMat(mat);
        System.out.println("mat[0][0]: " + mat[0][0]);
        System.out.println("mat[0][2]: " + mat[0][2]);
        System.out.println("mat[2][0]: " + mat[2][0]);
        System.out.println("mat[2][2]: " + mat[2][2]);

        int[][] m = new int[2][2];
        System.out.println("\nEmpty New Matrix: " + Arrays.deepToString(m) + "\n");
    }

    private static int[][] generateSquareMatrix(int size) {
        int[][] mat = new int[size][size];

        for (int i = 0; i < size * size; i++) {
            mat[i / size][i % size] = i;
        }

        return mat;
    }

    private static void printMat(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(mat[i][j] + "  ");
            }
            System.out.println("");
        }
    }

    private static void transposeMat(int[][] mat) {
        int tmp;
        for (int i = 0; i < mat.length; i++) {
            for (int j = i; j < mat[0].length; j++) {
                tmp = mat[i][j];
                mat[i][j] = mat[j][i];
                mat[j][i] = tmp;
            }
        }
    }

    private static int[][] multiplySquareMatrices(int[][] firstMatrix, int[][] secondMatrix) {
        int colsInFirstMatrix = firstMatrix[0].length;
        int rowsIntSecondMatrix = secondMatrix.length;
        if (colsInFirstMatrix != rowsIntSecondMatrix) {
            throw new RuntimeException("Number of columns in the first matrix has to be the same as number of rows in the second matrix");
        }

        int rowsInResultMatrix = firstMatrix.length;
        int colsInResultMatrix = secondMatrix[0].length;
        int[][] resultMatrix = new int[rowsInResultMatrix][colsInResultMatrix];

        for (int i = 0; i < rowsInResultMatrix; i++) {
            for (int j = 0; j < colsInResultMatrix; j++) {
                for (int k = 0; k < colsInFirstMatrix; k++) {
                    resultMatrix[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }
}