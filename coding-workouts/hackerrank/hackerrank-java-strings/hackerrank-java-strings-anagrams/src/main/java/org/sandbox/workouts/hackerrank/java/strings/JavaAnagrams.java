package org.sandbox.workouts.hackerrank.java.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Solution for: https://www.hackerrank.com/challenges/java-anagrams
 */
public class JavaAnagrams {

    public static void main(String[] args) {
        String s1 = "aab";
        String s2 = "aba";
        String s3 = "c";
        String s4 = "aaaaaaa";

        System.out.println("isAnagram('" + s1 + "', '" + s1 + "'): " + isAnagram(s1, s1));
        System.out.println("isAnagram('" + s1 + "', '" + s2 + "'): " + isAnagram(s1, s2));
        System.out.println("isAnagram('" + s1 + "', '" + s3 + "'): " + isAnagram(s1, s3));
        System.out.println("isAnagram('" + s1 + "', '" + s4 + "'): " + isAnagram(s1, s4));
    }

    /**
     * Return whether a string 'a' is an anagram of a string 'b'
     * @param a First string
     * @param b Second string
     * @return Whether the first string is anagram of the second
     */
    private static boolean isAnagram(String a, String b) {

        Map<Character, Integer> h1 = stringHistogram(a.toLowerCase());
        Map<Character, Integer> h2 = stringHistogram(b.toLowerCase());

        if (h1.size() != h2.size()) {
            return false;
        }

        for (Map.Entry<Character, Integer> entry : h1.entrySet()) {
            if (!h2.containsKey(entry.getKey())
                    || !h2.get(entry.getKey()).equals(entry.getValue())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets a string a returns a histogram of characters it consists of
     * @return A map which represents a string's histogram
     */
    private static Map<Character, Integer> stringHistogram(String s) {
        Map<Character, Integer> histogram = new HashMap<>();

        for (Character c : s.toCharArray()) {
            if (!histogram.containsKey(c)) {
                histogram.put(c, 1);
            } else {
                histogram.put(c, histogram.get(c) + 1);
            }
        }

        return histogram;
    }
}
