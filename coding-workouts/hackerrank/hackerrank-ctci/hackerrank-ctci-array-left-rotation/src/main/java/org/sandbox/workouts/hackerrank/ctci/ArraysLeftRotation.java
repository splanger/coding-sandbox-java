package org.sandbox.workouts.hackerrank.ctci;

import java.util.Scanner;

/**
 * A solution for https://www.hackerrank.com/challenges/ctci-array-left-rotation
 */
public class ArraysLeftRotation {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        for (int i = 0; i < k; i++) {
            rotate(a);
        }

        printArrInLine(a);
    }

    /**
     * Shift-lefts an array
     * @param arr Array to shift-left
     */
    private static void rotate(int[] arr) {
        if (arr.length > 1) {
            int tmp = arr[0];
            for (int i = 0; i < arr.length - 1; i++) {
                arr[i] = arr[i + 1];
            }
            arr[arr.length - 1] = tmp;
        }
    }

    /**
     * Prints out an array on a single line
     * @param arr Array to print out
     */
    private static void printArrInLine(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}