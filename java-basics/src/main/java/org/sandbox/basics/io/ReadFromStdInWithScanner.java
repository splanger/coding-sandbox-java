package org.sandbox.basics.io;

import java.lang.System;
import java.util.Scanner;

public class ReadFromStdInWithScanner {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();

        System.out.println("Read an integer: " + i);
    }
}
