package org.sandbox.basics.io;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

public class CopyCharacterStreamDemo {

	public static void main(String... args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: CopyCharacterStreamDemo <sourceFile> <destinationFile>");
			return;
		}
		
		String sourceFile = args[0];
		String destinationFile = args[1];

        try (
        	FileReader in =  new FileReader(sourceFile);
        	FileWriter out = new FileWriter(destinationFile);
        	) {
        	int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        }
	}
}