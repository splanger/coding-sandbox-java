package org.sandbox.basics.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyByteStreamDemo {

	public static void main(String... args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: CopyByteStreamDemo <sourceFile> <destinationFile>");
			return;
		}

		String sourceFile = args[0];
		String destinationFile = args[1];

        try (
        	FileInputStream in =  new FileInputStream(sourceFile);
        	FileOutputStream out = new FileOutputStream(destinationFile);
        	) {
        	int c;
            while ((c = in.read()) != -1) {
            	System.out.print(c + " | ");
                out.write(c);
            }
        }
	}
}