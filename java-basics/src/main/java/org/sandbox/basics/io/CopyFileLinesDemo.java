package org.sandbox.basics.io;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;

public class CopyFileLinesDemo {

	public static void main(String... args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: CopyCharacterStreamDemo <sourceFile> <destinationFile>");
			return;
		}
		
		String sourceFile = args[0];
		String destinationFile = args[1];

        try (
        	BufferedReader in =  new BufferedReader(new FileReader(sourceFile));
        	PrintWriter out = new PrintWriter(new FileWriter(destinationFile));
        	) {
        	String line;
            while ((line = in.readLine()) != null) {
                out.println(line);
            }
        }
	}
}