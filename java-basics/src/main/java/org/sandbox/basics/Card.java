package org.sandbox.basics;


public class Card {

    // Card Ranks
    public static final int ACE = 1;
    public static final int DEUCE = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final int JACK = 11;
    public static final int QUEEN = 12;
    public static final int KING = 13;

    // Card Suites
    public static final int DIAMONDS = 1;
    public static final int CLUBS = 2;
    public static final int HEARTS = 3;
    public static final int SPADES = 4;

    private final int rank;
    private final int suite;

    public Card(int rank, int suite) {
        assert isValidRank(rank);
        assert isValidSuite(suite);
        this.rank = rank;
        this.suite = suite;
    }

    public int getSuite() {
        return suite;
    }

    public int getRank() {
        return rank;
    }

    private static boolean isValidRank(int rank) {
        return rank >= ACE && rank <= KING;
    }

    private static boolean isValidSuite(int suite) {
        return suite >= 1 && suite <= 4;
    }
}
