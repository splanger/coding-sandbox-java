package org.sandbox.basics;


public class CardDeck {
    // 4 - Amount of suites, 13 - amount of ranks
    private Card[] cards = new Card[4 * 13];

    public CardDeck() {
        for (int rank = Card.ACE; rank <= Card.KING; rank++) {
            for (int suite = Card.DIAMONDS; suite <= Card.SPADES; suite++) {
                cards[rank * suite] = new Card(rank, suite);
            }
        }
    }
}
