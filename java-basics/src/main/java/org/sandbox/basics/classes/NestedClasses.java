package org.sandbox.basics.classes;


public class NestedClasses {
    public static void main(String[] args) {
        OuterClass.NestedStaticClass nsc = new OuterClass.NestedStaticClass();
        nsc.sayHello();
        OuterClass.NestedStaticClass.sayStaticHello();

        OuterClass a = new OuterClass();
        OuterClass.InnerClass ic = a.new InnerClass();
        ic.sayHello();
        ic.sayOuterHello();

        System.out.println("Local class: 2 + 2 = " + OuterClass.testLocalClass(2, 2));
    }
}

class OuterClass {

    private static final int AGE = 25;
    private static int numInstances = 0;
    private String name = "Vasya";

    public OuterClass() {
        numInstances++;
    }

    public OuterClass(String name) {
        this();
        this.name = name;
    }

    public static void sayStaticHello() {
        System.out.println("OuterClass: Hello! My Age is " + AGE + ". There're " + numInstances + " instances of me. Though I can't tell you their names.");
    }

    public void sayHello() {
        System.out.println("Hey, I'm instance of OuterClass. My name is " + name + ". The age of OuterClass is " + AGE + " and of NestedStaticClass is " + NestedStaticClass.AGE);
    }

    public static int testLocalClass(int num1, int num2) {

        class WrongCalculator {
            public static final int CONSTANT_ERROR = 1;
            public int add() {
                return num1 + num2 + CONSTANT_ERROR;
            }
        }

        WrongCalculator calc = new WrongCalculator();

        return calc.add();
    }

    static class NestedStaticClass {
        private static final int AGE = 20;
        private static int numInstances = 0;
        private String name = "Petya";

        public NestedStaticClass() {
            numInstances++;
        }

        public static void sayStaticHello() {
            System.out.println("OuterClass.NestedStaticClass: Hello! My Age is " + AGE + ". There're " + numInstances + " instances of me. Outer class's age is " + OuterClass.AGE + " and it has " + OuterClass.numInstances + " instances, though I can't tell you their names.");
        }

        public void sayHello() {
            System.out.println("Hi, I'm instance of OuterClass.NestedStaticClass's sayHello(). The OuterClass's age is " + OuterClass.AGE + ". I can't access the OuterClass's non-static fields unless its instance was passed directly.");
        }
    }

    class InnerClass {

        /* Inner class can't have static members */
        private String name = "Nikolai";

        public InnerClass getInstance() {
            return this;
        }

        public void sayHello() {
            System.out.println("OuterClass.this.InnerClass: Hello! My name is + " + this.name + " and I'm " + AGE + " years old. There're " + numInstances + " instances of me. The OuterClass's instance name is " + OuterClass.this.name);
        }

        public void sayOuterHello() {
            System.out.println("OuterClass.this.InnerClass.sayHello() :  ");
            OuterClass.this.sayHello();
        }
    }

}