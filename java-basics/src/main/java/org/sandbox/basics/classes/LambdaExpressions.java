package org.sandbox.basics.classes;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaExpressions {

    public static void main(String[] args) {
        List<Person> roster = new ArrayList<>();
        roster.add(new Person("Vasya", LocalDate.now(), Person.Sex.MALE, "vasya@gmail.com"));
        roster.add(new Person("Petya", LocalDate.now(), Person.Sex.MALE, "petya@gmail.com"));

        System.out.println("Approach 6: Use Standard Functional Interfaces with Lambda Expressions\n");
        Person.printPersonsWithPredicate(
                roster,
                p -> p.getGender() == Person.Sex.MALE
        );

        System.out.println("Approach 7: Use Lambda Expressions Throught Your Application\n");
        Person.processPersons(
                roster,
                p -> p.getGender() == Person.Sex.MALE,
                Person::getEmailAddress,
                System.out::println
        );

        System.out.println("Approach 8: Use Generics More Extensively\n");
        Person.processElements(
                roster,
                p -> p.getGender() == Person.Sex.MALE,
                Person::getEmailAddress,
                System.out::println
        );

        System.out.println("Approach 9: Use Aggregate Operations That Accept Lambda Expressions as Parameters\n");
        roster
                .stream()   // Obtains a source of objects;                  Stream<E> stream()
                .filter(    // Filter objects that match a Predicate object; Stream<T> filter(Predicate<? super T> predicate)
                    p -> p.getGender() == Person.Sex.MALE)
                .map(Person::getEmailAddress)      //
                .forEach(System.out::println);
    }

    static class Person {

        public enum Sex {
            MALE, FEMALE
        }

        private String name;
        private LocalDate birthday;
        private Sex gender;
        private String emailAddress;

        public Person(String name, LocalDate birthday, Sex gender, String emailAddress) {
            this.name = name;
            this.birthday = birthday;
            this.gender = gender;
            this.emailAddress = emailAddress;
        }

        public String getEmailAddress() {
            return emailAddress;
        }

        public void setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        public Sex getGender() {
            return gender;
        }

        public void setGender(Sex gender) {
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public LocalDate getBirthday() {
            return birthday;
        }

        public void setBirthday(LocalDate birthday) {
            this.birthday = birthday;
        }

        public void printPerson() {
            System.out.println(name + " " + emailAddress);
        }

        public static void printPersonsWithPredicate(List<Person> roster,
                                                      Predicate<Person> tester) {
            for (Person p : roster) {
                if (tester.test(p)) {
                    p.printPerson();
                }
            }

        }

        public static void processPersons(List<Person> roster,
                                          Predicate<Person> tester,
                                          Function<Person, String> mapper,
                                          Consumer<String> block) {
            for (Person p : roster) {
                if (tester.test(p)) {
                    String data = mapper.apply(p);
                    block.accept(data);
                }
            }
        }

        public static <X, Y> void processElements(Iterable<X> roster,
                                          Predicate<X> tester,
                                          Function<X, Y> mapper,
                                          Consumer<Y> block) {
            for (X p : roster) {
                if (tester.test(p)) {
                    Y data = mapper.apply(p);
                    block.accept(data);
                }
            }
        }
    }
}
