package org.sandbox.basics.classes;

/**
 * This class demonstrates using Lambda Expressions to pass functionality
 */
public class LambdaCalculator {

    public static void main(String[] args) {
        BinaryCalculator calc = new BinaryCalculator();
        IntegerMath addition = (a, b) -> a + b;
        IntegerMath subtraction = (a, b) -> a - b;

        System.out.println("25 + 47 = "  + calc.performOperation(25, 47, addition));
        System.out.println("137 - 31 = " + calc.performOperation(137, 31, subtraction));
    }

    /**
     * Represents a functional interface with gets 2 integers and returns an integer
     */
    interface IntegerMath {
        /**
         * Gets two parameters, performs math operation on them and returns the result
         * @param a First number
         * @param b Second number
         * @return Result of the opperation
         */
        int opperate(int a, int b);
    }

    /**
     * Binary calculator can perform any math operation on 2 integers
     */
    static class BinaryCalculator {
        public int performOperation(int a, int b, IntegerMath op) {
            return op.opperate(a, b);
        }
    }
}
