package org.sandbox.algorithms;

import org.sandbox.algorithms.sorting.SortingAlgorithm;
import org.sandbox.algorithms.sorting.SortingAlgorithmsFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class SortingAlgorithmsDemo {

    public static void main(String... args) {
        Integer[] array;
        if (args.length > 1) {
            array = parseIntegersArrayFromStringsArray(args);
        } else if (args.length == 1) {
            array = generateIntegersArray(Integer.parseInt(args[0]));
        } else {
            array = generateIntegersArray(100000);
        }

        Integer[] tmp = array.clone();

        System.out.println("Sorting an array with " + array.length + " values");
        if (args.length > 1) {
            System.out.println("Array before sorting:" + Arrays.toString(tmp));
        }

        List<SortingAlgorithm> sortingAlgorithms = SortingAlgorithmsFactory.createAll();

        long startTime;
        long stopTime;
        for (SortingAlgorithm algo : sortingAlgorithms) {
            startTime = System.nanoTime();
            algo.sort(tmp);
            stopTime = System.nanoTime();
            System.out.print(algo.getClass().getSimpleName()
                            + " (took: " + ((stopTime - startTime) / 1000000) + " ms)");

            if (args.length > 1) {
                System.out.print(": ");
                System.out.println(Arrays.toString(tmp));
            } else {
                System.out.println("");
            }

            tmp = array.clone();
        }
    }

    private static Integer[] parseIntegersArrayFromStringsArray(String... s) {
        Integer[] arr = new Integer[s.length];
        for (int i = 0; i < s.length; i++) {
            arr[i] = Integer.parseInt(s[i]);
        }

        return arr;
    }

    private static Integer[] generateIntegersArray(int arrayLength) {
        if (arrayLength < 0) {
            throw new RuntimeException("Array length must be greater than 0");
        }

        Integer[] arr = new Integer[arrayLength];
        Random rand = new Random();
        for (int i = 0; i < arrayLength; i++) {
            arr[i] = rand.nextInt();
        }

        return arr;
    }
}

