package org.sandbox.algorithms.sorting;


import java.util.Arrays;

public class MergeSort implements SortingAlgorithm {
	
	public <T extends Comparable<? super T>> void sort(T[] arr) {
		if (arr.length < 2) {
			return;
		}

		T[] leftSubArray = Arrays.copyOf(arr, arr.length / 2);
		T[] rightSubArray = Arrays.copyOfRange(arr, arr.length / 2, arr.length);

		sort(leftSubArray);
		sort(rightSubArray);

		merge(arr, leftSubArray, rightSubArray);
	}

	@SuppressWarnings("unchecked")
	private <T extends Comparable<? super T>> void merge(T[] arr, T[] leftSubArray, T[] rightSubArray) {
		int i = 0;
		int j = 0;
		int k = 0;

		while (i < leftSubArray.length && j < rightSubArray.length) {
			if (leftSubArray[i].compareTo(rightSubArray[j]) < 0) {
				arr[k] = leftSubArray[i];
				i++;
			} else if (leftSubArray[i].compareTo(rightSubArray[j]) > 0) {
				arr[k] = rightSubArray[j];
				j++;
			} else {
				arr[k] = leftSubArray[i];
				k++;
				arr[k] = rightSubArray[j];
				i++;
				j++;
			}
			k++;
		}

		// Copy the remaining values
		while (i < leftSubArray.length) {
			arr[k] = leftSubArray[i];
			i++;
			k++;
		}

		while (j < rightSubArray.length) {
			arr[k] = rightSubArray[j];
			j++;
			k++;
		}
	}
}