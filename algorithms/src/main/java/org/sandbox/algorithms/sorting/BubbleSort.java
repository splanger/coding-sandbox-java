package org.sandbox.algorithms.sorting;


public class BubbleSort implements SortingAlgorithm {

    public <T extends Comparable<? super T>> void sort(T[] array) {
        if (array.length < 2) {
            return;
        }

        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].compareTo(array[i + 1]) > 0) {
                    swapElements(array, i, i + 1);
                    sorted = false;
                }
            }
        }
    }

    private static void swapElements(Object[] arr, int firstIndex, int secondIndex) {
        Object tmp = arr[firstIndex];
        arr[firstIndex] = arr[secondIndex];
        arr[secondIndex] = tmp;
    }
}