package org.sandbox.algorithms.sorting;


import java.util.Arrays;

public class QuickSort implements SortingAlgorithm {
	
	public <T extends Comparable<? super T>> void sort(T[] arr) {
		if (arr.length < 2) {
			return;
		}
	}

	public <T extends Comparable<? super T>> void partition(T[] arr) {
		T pivot = arr[arr.length - 1];

		int low = 0;
		int high = arr.length - 2;
		while (low < high) {
			if (arr[low].compareTo(pivot) < 0) {
				low++;
			}
			if (arr[high].compareTo(pivot) > 0) {
				high--;
			}
			if (arr[low].compareTo(pivot) >= 0 && arr[high].compareTo(pivot) < 0) {
				arr[arr.length - 1] = arr[low];
				arr[low] = arr[high];
				arr[high] = arr[arr.length - 1];
				low++;
				high--;
			}
		}

		arr[arr.length - 1] = arr[low];
		arr[low] = pivot;
	}
}