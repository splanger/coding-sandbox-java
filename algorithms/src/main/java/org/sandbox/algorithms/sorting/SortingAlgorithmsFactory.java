package org.sandbox.algorithms.sorting;

import java.util.List;
import java.util.ArrayList;

public class SortingAlgorithmsFactory {

    public static List<SortingAlgorithm> createAll() {
        List<SortingAlgorithm> l = new ArrayList<>();

        l.add(new BubbleSort());
        l.add(new InsertionSort());
        l.add(new MergeSort());

        return l;
    }
}
