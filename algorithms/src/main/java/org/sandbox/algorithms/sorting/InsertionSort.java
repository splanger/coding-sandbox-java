package org.sandbox.algorithms.sorting;


public class InsertionSort implements SortingAlgorithm {

    public <T extends Comparable<? super T>> void sort(T[] array) {
        if (array.length < 2) {
            return;
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    swap(array, j, j + 1);
                } else {
                    break;
                }
            }
        }
    }

    private static void swap(Object[] arr, int firstIndex, int secondIndex) {
        Object tmp = arr[firstIndex];
        arr[firstIndex] = arr[secondIndex];
        arr[secondIndex] = tmp;
    }
}