import org.junit.Test;
import org.sandbox.algorithms.sorting.QuickSort;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class TestQuickSortAlgorithm {

    @Test
    public void canCreateQuickSort() throws Exception {
        QuickSort algo = new QuickSort();
    }

    @Test
    public void testSortEmptyArray() throws Exception {
        Integer[] arr = {};

        QuickSort algo = new QuickSort();
        algo.sort(arr);
        assertEquals(arr.length, 0);
    }

    @Test
    public void testSortArrayWithOneElement() throws Exception {
        Integer[] arr = {1};

        QuickSort algo = new QuickSort();
        algo.sort(arr);
        assertEquals(arr.length, 1);
        assertEquals(arr[0].intValue(), 1);
    }

    @Test
    public void testCanPartitionSubArrayOfEvenSize() {
        Integer[] arr = {3, 1, 6, 7, 8, 5};
        Integer[] expectedArrAfterPartitioning = {3, 1, 5, 7, 8, 6};

        QuickSort qs = new QuickSort();
        qs.partition(arr);

        System.out.println(Arrays.toString(arr));

        for (int i = 0; i < arr.length; i++) {
            assertEquals(arr[i].compareTo(expectedArrAfterPartitioning[i]), 0);
        }
    }

    /*@Test
    public void testCanPartitionSubArrayOfOddSize() {
        Integer[] arr = {3, 1, 6, 7, 5};
        Integer[] expectedArrAfterPartitioning = {3, 1, 5, 7, 6};

        QuickSort qs = new QuickSort();
        qs.partition(arr);

        System.out.println(Arrays.toString(arr));

        for (int i = 0; i < arr.length; i++) {
            assertEquals(arr[i].compareTo(expectedArrAfterPartitioning[i]), 0);
        }
    }*/

    /*@Test
    public void testSortArrayWithManyElements() throws Exception {
        Integer[] arr = {7, 3, 5, 2, 1};
        Integer[] expectedSortedArray = {1, 2, 3, 5, 7};

        BubbleSort algo = new BubbleSort();
        algo.sort(arr);
        assertEquals(arr.length, 5);

        for (int i = 0; i < expectedSortedArray.length; i++) {
            assertEquals(expectedSortedArray[i].intValue(), arr[i].intValue());
        }
    }*/
}
