import org.junit.Test;
import org.sandbox.algorithms.sorting.MergeSort;


import static org.junit.Assert.assertEquals;

public class TestMergeSortAlgorithm {

    @Test
    public void testSortEmptyArray() throws Exception {
        Integer[] arr = {};

        MergeSort algo = new MergeSort();
        algo.sort(arr);
        assertEquals(arr.length, 0);
    }

    @Test
    public void testSortArrayWithOneElement() throws Exception {
        Integer[] arr = {1};

        MergeSort algo = new MergeSort();
        algo.sort(arr);
        assertEquals(arr.length, 1);
        assertEquals(arr[0].intValue(), 1);
    }

    @Test
    public void testSortArrayWithManyElements() throws Exception {
        Integer[] arr = {7, 3, 5, 2, 1};
        Integer[] expectedSortedArray = {1, 2, 3, 5, 7};

        MergeSort algo = new MergeSort();
        algo.sort(arr);
        assertEquals(arr.length, 5);

        for (int i = 0; i < expectedSortedArray.length; i++) {
            assertEquals(expectedSortedArray[i].intValue(), arr[i].intValue());
        }
    }
}
