import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.sandbox.algorithms.sorting.BubbleSort;

import java.util.Arrays;
import java.util.Random;

public class TestBubbleSortAlgorithm {

    @Test
    public void testSortEmptyArray() throws Exception {
        Integer[] arr = {};

        BubbleSort algo = new BubbleSort();
        algo.sort(arr);
        assertEquals(arr.length, 0);
    }

    @Test
    public void testSortArrayWithOneElement() throws Exception {
        Integer[] arr = {1};

        BubbleSort algo = new BubbleSort();
        algo.sort(arr);
        assertEquals(arr.length, 1);
        assertEquals(arr[0].intValue(), 1);
    }

    @Test
    public void testSortArrayWithManyElements() throws Exception {
        Integer[] arr = {7, 3, 5, 2, 1};
        Integer[] expectedSortedArray = {1, 2, 3, 5, 7};

        BubbleSort algo = new BubbleSort();
        algo.sort(arr);
        assertEquals(arr.length, 5);

        for (int i = 0; i < expectedSortedArray.length; i++) {
            assertEquals(expectedSortedArray[i].intValue(), arr[i].intValue());
        }
    }

    @Test
    public void testIntegerDivisionInJava() {
        System.out.println("In Java integer 7/2 = " + (7 / 2) + " (real result 3.5)");
        System.out.println("In Java integer 11/6 = " + (11 / 6) + " (real result 1.83333)");
    }

    @Test
    public void testJavaSubArray() {
        int arrLength = 7;
        MyNumber[] arr = new MyNumber[arrLength];

        // Generate MyNumbers
        Random rand = new Random();
        for (int i = 0; i < arrLength; i++) {
            arr[i] = new MyNumber(rand.nextInt(100));
        }

        // Test sub array
        MyNumber[] leftSubArray = Arrays.copyOf(arr, arr.length / 2);
        MyNumber[] rightSubArray = Arrays.copyOfRange(arr, arr.length / 2, arr.length);

        System.out.println("Original array: " + Arrays.toString(arr));
        System.out.println("Left sub-array: " + Arrays.toString(leftSubArray));
        System.out.println("Right sub-array: " + Arrays.toString(rightSubArray));
        System.out.println("Class of leftSubArray[0] " + leftSubArray[0].getClass().getSimpleName());

    }

    static class MyNumber {
        private int value;

        public MyNumber(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value + "";
        }
    }
}
